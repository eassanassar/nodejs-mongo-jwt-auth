const image = require('../helpers/functions/images');
const usersHelper = require('../helpers/functions/usersHelper');
const User = require('../models/user');
const randomString = require('randomstring');
const mailer = require('../helpers/functions/mailer');
const googleOauth = require('../helpers/functions/googleOauth');
const features = require('../config/features');


exports.signup = (async (req, res, next)=>{
try{
    const user = await User.findOne({'local.email': req.value.body.email});
    //if user exist
    if(user){
      throw new Error('E-Mail Exists');
    }
    //hash the password
    const hash = await usersHelper.hashPassword(req.value.body.password);
    let newUser = new User({
      authMethod: 'local',
      local: {
        email: req.value.body.email.toLowerCase(),
        password: hash,
      }
    });
    let successMsg = 'User created successfuly';

    //email verification
    if(features.allowEmailVerification){
      //generate secret token for email verification
      const secretToken = randomString.generate();
      // the html to send to email verification
      const verifyEmail = require('../helpers/mails/verifyEmail')
      const html = verifyEmail.verifyEmailHTML(secretToken);
      const subject = verifyEmail.subject;

      newUser.local.secretToken = secretToken;
      newUser.local.valid = false;
      const email = await mailer.sendEmail(process.env.FROM_EMAIL, req.value.body.email, subject, html);
      successMsg += ', Need to verify Email';
    }else{
      newUser.local.valid = true;
    }

  //save new user
    const userSuccess = await newUser.save();
    if(userSuccess){
      res.status(200).json({
        message: successMsg
      });
    }

  }catch(err){
    console.error(err);
    res.status(500).json({
      error: 'Auth Error'
    });
  }
});
//local login
exports.login = (async (req, res, next)=>{
  try {
    const user = await User.findOne({'local.email': req.value.body.email.toLowerCase()});
    if(user === null){
      throw new Error('user does not exist');
    }
    const local = user.local;
    //if it was not validated by email
    if(!local.valid){
      throw new Error('email is not verified');
    }

    //check if recieved password is the same as the local password
    const comparePassword = await usersHelper.comparePassword(req.value.body.password, local.password);
    // if login auth success
    if (comparePassword){
      const token = usersHelper.signToken({email: local.email, userID: user._id});
      return res.status(200).json({
        token,
        message: 'Auth successful'
      });
    }
    else{
      throw new Error(`userCtrl - login - password do not match`);
    }
  }catch(err){
    console.error(`Local Login Errors : ${err}`)
    res.status(500).json({
      message: 'Auth Failed'
    });
  }
});

exports.uploadAvatar = (req, res, next)=>{
  image.resizeImg(req.file, "avatar").then((result)=>{
    console.log(result);
    res.status(200).json({});
  }).catch(err =>{
    console.error(err);
    res.status(500).json({error: err});
  });
};

exports.loginGoogle = (async (req, res, next)=>{
  try{
    const code = req.query.code;

    return googleOauth.getGoogleData(code).then((async (data) =>{
      const existingUser = await User.findOne({'google.id': data.id});

      if(existingUser){
        const token = usersHelper.signToken({method: 'google', email: existingUser.google.email, userID: existingUser._id});
        return res.status(200).json({
          token,
          message: 'Auth successful'
        });
      }else{
        // if user does not exist create one and sign in

        const newUser = new User({
          authMethod: 'google',
          google:{
            id: data.id,
            email: data.emails[0].value
          }
        });
        const savedUser = await newUser.save();
        if(savedUser){
          console.log(savedUser);
          const token = usersHelper.signToken({method: 'google', email: savedUser.email, userID: savedUser._id});
          return res.status(200).json({
            token,
            message: 'Auth successful'
          });
        }
      }
      //res.status(200).json({data});
    })).catch(error =>{
      console.error(`loginGoogle - googleOauth.getGoogleData ${error}`);
      return res.status(500).json({message: 'Auth Failed'});
    });
  }catch(err){
    console.error(`usersCtrl - ${err}`);
    return res.status(500).json({message: 'Auth Failed'});
  };
});




exports.delete = (req, res, next)=>{
  console.log(req.param.userId)
  User.remove({'local._id': req.param.userId})
  .exec()
  .then(result =>{
    res.status(200).json({
      message: 'User was deleted successfuly'
    });
  })
  .catch(err =>{
    console.error(err)
    res.status(500).json({
      message: err,
      type: 'error'
    });
  })
};

// post - email
exports.sendRetrivePassword = (async (req, res, next)=>{
  //TODO: validate email string
    try{
      const user = await User.findOne({'local.email': req.body.email});

      if(user){
        //generate secret token for email verification
        const secretToken = randomString.generate();
        // the html to send to email verification
        const resetPasswordEmail = require('../helpers/mails/resetPasswordEmail')
        const html = resetPasswordEmail.emailHTML(secretToken);
        const subject = resetPasswordEmail.subject;
      //  console.log(`email: ${email}`)
        user.local.resetPasswordToken = {secretToken, expire: Date.now() + 36000000}; // 1hour
        console.log(user.local.resetPasswordToken )
        const email = await mailer.sendEmail(process.env.FROM_EMAIL, req.body.email, subject, html);
        await user.save();
      }

      // send 'it was sent' even if it didn't beacause i don't want anyone to know if the email exist in the database or not
      res.status(200).json({
        message: 'An email was sent',
        type: 'message'
      })

  }catch(err){
    res.status(500).json({
      message: err,
      type: 'error'
    });
  }
});

// verify the token is not expired and save new password
// post -(secretToken, password)
exports.resetPassword = (async (req, res, next)=>{
  try{
    const user = await User.findOne({'local.resetPasswordToken.secretToken': req.body.secretToken, 'local.resetPasswordToken.expire': {$gt: Date.now()}});
    console.log(user);

    if(user){
      console.log(`old password : ${user.local.password}`);
      const secretToken = req.body.secretToken;
      const password = req.body.password;
      const hash = await usersHelper.hashPassword(password);
      user.local.password = hash;
      user.local.resetPasswordToken = undefined;
      const savedUser = await user.save();
      console.log(`new password : ${savedUser.local.password}`);

      //TODO: set the html and subject in diffrent file
      const html =` Hi there,
              <br>
              You've successfully changed your LinkedIn password.
              `
      const email = await mailer.sendEmail(process.env.FROM_EMAIL, user.local.email, 'your password was successfully reset', html);

      //TODO: change it to redirect to login page
      res.status(200).json({
        message: 'password was reset',
        type: 'message'
      });

    }else{
      res.status(401).json({
        message: 'User was not found Or token expired',
        type: 'warning'
      });
    }
  }catch(err){
    res.status(500).json({
      message: err,
      type: 'error'
    });
  }
});

//verify email (set it as valid`)
//parameters needed : post ->  secretToken
exports.verify= (async (req, res, next)=>{
  const secretToken = req.body.secretToken;
  //TODO: create a middleware to validate input and remove this
  if(secretToken === undefined || secretToken === ''){
    return res.status(500).json({
      message: 'token is not defined'
    });
  }

  const user = await User.findOne({'local.secretToken': secretToken});
  if(!user){
    return res.status(401).json({
      message: 'token was not found or expired'
    });
    //TODO: redirect to verify email html page
    //res.redirect('/users/verify')
  }

  user.local.valid = true;
  user.local.secretToken = '';
  user.save().then(result =>{
    return res.status(200).json({
      message: 'user was verified successfully'
    });
    //TODO: redirect to login page
    //res.redirect('/users/login');
  }).catch(err=>{
    return res.status(500).json({
      message: 'Failed to save and validate user email'
    });
    //TODO: redirect to verify email html page
    //res.redirect('/users/verify')
  })

})
