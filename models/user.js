const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  authMethod: {
    type: String,
    enum: ['local', 'google', 'facebook'],
    required: true
  },
  //TODO: change to custom role
  role: {
    type: String,
    enum: ['admin', 'moderator', 'user'],
    default: 'user',
    required: true
  },
  local: {
    email: {
      type: String,
      lowercase:true,
      match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
     },
     resetPasswordToken:{
       secretToken: {type: String},
       expire: {type: Date}
     },
    password: {type: String},
    secretToken: {type: String},
    valid: {type: Boolean}
  },
  google:{
    id: {type: String},
    email: {
      type: String,
      lowercase:true,
      match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
     },
  },
  facebook:{
    id: {type: String},
    email: {
      type: String,
      lowercase:true,
      match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
     },
  }

});

module.exports = mongoose.model('user', userSchema)
