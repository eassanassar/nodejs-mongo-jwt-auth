const express = require ('express');
const app = express();
const helmet = require('helmet')
//for logging
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const userRoutes = require('./api/routes/users');

//mongoose.Promise = global.Promise;
mongoose.connect(process.env.DB_LINK);
//test connection
mongoose.connection.once('open', function(){
    console.log('mongoDB connected sucessfully !!');
}).on('error', function(error){
    console.log('MongoDB Connection error: ', error);
});

app.use(helmet({
  hsts: false
}));

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//prevent CORS error , in order to allow apps and other server to connect to this server
app.use((req, res, next) =>{
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-hEADERS', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  if(req.method === 'OPTIONS'){
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH');
    return res.status(200).json({});
  }
  next();

});

app.use('/public', express.static('public'));
// Routes
app.use('/users', userRoutes);

//404 if couldnt find route
app.use((req, res, next) =>{
    const error = new Error('Route not found');
    error.status = 404;
    next(error)
});

// Error handling
app.use((error, req, res, next)=>{
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
})

module.exports = app;
