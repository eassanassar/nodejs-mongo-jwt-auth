//multer parse json and images
const multer = require('multer');
const sharp = require('sharp');
const config = require('../../config/main.js')
/*
// if wants to upload directly to destination without meddeling with the file use this function instaed of  multer.memoryStorage()
const storageOptions = (path)=>{
  return multer.diskStorage({
    destination: function(req, file, cb){
      cb(null, path)
    },
    filename: function(req, file, cb){
      cb(null,  `${Date.now()}_${file.originalname}`);
    },

  });
};
*/

//filter to only be able to upload pics
const fileFilter = (req, file, cb)=>{
  if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
    //set to true to save file
    cb(null, true);
  }else{
    cb(new Error(`File not supported ${file.mimetype}`), false);
  }
}

/**
 multerFile:
     {
        "fieldname": "avatar",
        "originalname": "examples.png",
        "encoding": "7bit",
        "mimetype": "image/png",
        "buffer": {
            "type": "Buffer",
            "data": []
          }
      }
**/
const resizeImgCustom = (multerFile, width, height, path )=>{
         return new Promise((resolve, reject)=>{
           //resize and save image
           sharp(multerFile.buffer).resize(width, height).toFile(`${path}/${Date.now()}_${multerFile.originalname}`, (err, info) =>{
             if(err){
               reject(new Error(`helper/functions/images - resizeImgCustom error: ${err}`));
             }
             else{
               resolve(`File was uploaded to ${path}/${Date.now()}_${multerFile.originalname}`);
             }
           });
       })
};

const resizeImg =  (multerFile, type)=>{
  let imgConf = config.IMAGE[type];
  return resizeImgCustom(multerFile, imgConf.width, imgConf.height, imgConf.path )
}

module.exports = {
  upload :  multer({
   storage: multer.memoryStorage(),//storageOptions(path),
   //upload up 0.5 mb
   fileSize: 1024 * 1024 * 2,
   fileFilter: fileFilter,
 }),
 resizeImg: resizeImg
};
