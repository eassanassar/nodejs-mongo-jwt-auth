const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken');


const hashPassword = (password)=>{
  return bcrypt.hash(password, 10);
};

const comparePassword = (newPass , oldPass) =>{
  return bcrypt.compare(newPass, oldPass);
}

const signToken = (obj)=>{
  return jwt.sign(
    obj,
    process.env.JWT_KEY,
    {
      expiresIn: "1h"
    }
  );
}

module.exports= {
  hashPassword,
  comparePassword,
  signToken
}
