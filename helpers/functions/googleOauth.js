
//GOOGLE APIS OATH2
const {google} = require('googleapis');
const plus = google.plus('v1');


const oauth2Client = new google.auth.OAuth2(
  '875959079657-13sfemsa9f89b2ojgebksu9iblc6o3f6.apps.googleusercontent.com',
  'PfdO7eljd-4XZmLxryVN_W43',
  'http://localhost:3000/oathcallback'
);

// generate a url that asks permissions for Google+ and Google Calendar scopes
const scopes = [
'https://www.googleapis.com/auth/plus.me',
'https://www.googleapis.com/auth/userinfo.email',
'https://www.googleapis.com/auth/userinfo.profile'
];

const url = oauth2Client.generateAuthUrl({
  // 'online' (default) or 'offline' (gets refresh_token)
//  access_type: 'offline',
  // If you only need one scope you can pass it as a string
  scope: scopes
});

//code : Authorization code we get from google
const getGoogleData =  (code) => {
  return new Promise((resolve, reject)=>{
    try{
        oauth2Client.getToken(code, (err, tokens) => {
            if (err) {
              reject(err)
            }

          /*
            // Retrieve tokens via token exchange explained above or set them:
            oauth2Client.setCredentials({
            access_token: 'ya29.Glu6Bav1cCfPh-4YwzCFy1a2U_74ZZsOCu_oTD8N4gpaiJ_kLXetTeIJfd_5rm8I8CcN20Zhc_FMmsGAORDMVaAkOrtKH-cLicsNVzRgDkOUv9-JeL6BAjEmXQWx',
            refresh_token: '1/Y-P2MUL0b2vtroj7XrorLT0uiFzN0Bj5IKhaWYLEghw'
            // Optional, provide an expiry_date (milliseconds since the Unix Epoch)
            // expiry_date: (new Date()).getTime() + (1000 * 60 * 60 * 24 * 7)
          });
          */
          oauth2Client.setCredentials(tokens);

          //data is in response.data
           plus.people.get({
            userId: 'me',
            auth: oauth2Client
          }, (err, response)=>{
            if(err){
              reject(err);
            }else{
              resolve(response.data);
            }
          });
        });
      }catch(err){
        reject(err);
      }
  });
};


module.exports= {
  getGoogleData
};
