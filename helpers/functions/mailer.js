const nodemailer = require('nodemailer');
const config= require('../../config/mailer.js')


const transport = nodemailer.createTransport({
  service: 'Mailgun',
  auth:{
    user: config.MAILGUN_USER, //default smtp login
    pass: config.MAILGUN_PASS //default password
  },
  tls:{
    rejectUnauthorized: false
  }
});

const sendEmail = (from, to , subject, html)=>{
  return new Promise((resolve, reject)=>{
    return transport.sendMail({from, subject, to, html}, (err, info)=>{
      if(err){
        reject(err);
      }else{
        resolve(info)
      }
    });
  });
};

module.exports = {
  sendEmail
};
