const verifyEmailHTML= secretToken =>{
  return
  `Hi there,
  <br>
  Thank you for registering!
  <br><br>
  <form method="post" action="${process.env.DOMAIN}users/verify" style="display: inline;">
    <input type="hidden" name="secretToken" value="${secretToken}">
    <button type="submit" name="submit_param" value="submit_value" class="link-button" style="background: none; border: none; color: blue; text-decoration: underline; cursor: pointer; font-size: 1em; font-family: serif;">
      Click here to Validate Email !!
    </button>
  </form>
  `
};


const subject = 'Verify email'

module.exports = {
  emailHTML: verifyEmailHTML,
  subject
}
