const emailHTML= secretToken =>{
  //TODO: set path to be redirected to (html)
  //TODO: change it to send token by get method
  return `

Hi there,
<br>
To reset your password, please click the following link:
<br>
<a href="${process.env.DOMAIN}users/resetpassword?t=${secretToken}">${process.env.DOMAIN}users/resetpassword?t=${secretToken}</a>

<br>
If you don't want to reset your password, you can ignore this message - someone probably typed in your username or email address by mistake.
<br>
Thanks!<br><br>
`
};

const subject = 'Reset Password'

module.exports = {
  emailHTML,
  subject
}
