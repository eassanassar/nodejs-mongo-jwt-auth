const jwt = require('jsonwebtoken');


const isLogedin = (req, res, next)=>{
  try{
    // token is sent in the header as Authorization: bearer ${token}
    const token = req.headers.authorization.split(" ")[1]
    //verify(...) surves to verify and decode
    const decode = jwt.verify(token, process.env.JWT_KEY);
    next();
  }catch(err){
    console.info("token is not valid");
    return res.status(401).json({
      message: 'Auth failed',
      token: req.body.token
    });
  }
};


module.exports = {
  isLogedin: isLogedin
};
