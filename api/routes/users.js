const express = require('express');
const router = express.Router();
const usersCtrl = require('../../controllers/usersCtrl');
const image = require('../../helpers/functions/images');
const {validateBody, schemas} = require('../../helpers/middleware/validator');
const auth = require('../../helpers/middleware/check-auth');


router.post('/upload', auth.isLogedin, image.upload.single('avatar'), usersCtrl.uploadAvatar);
router.post('/signup', validateBody(schemas.authSchema), usersCtrl.signup);
router.post('/login', validateBody(schemas.authSchema), usersCtrl.login);
router.get('/login/google', usersCtrl.loginGoogle);
router.post('/verify', usersCtrl.verify);

//send token to email
router.post('/forgot', usersCtrl.sendRetrivePassword);
//if token not expired change password 
router.post('/resetpassword', usersCtrl.resetPassword);



/*
//TODO: only admin should be able to  delete ,
router.delete("/:userId", auth.isLogedin, usersCtrl.delete);
*/

module.exports = router;
